package com.tunmii.TransactionalDemo.Service;

import com.tunmii.TransactionalDemo.Entity.Company;
import com.tunmii.TransactionalDemo.Entity.Employee;
import com.tunmii.TransactionalDemo.Repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    CompanyService companyService;

//    public Employee getEmployeeById(int id) {
//        return employeeRepository.findById(id);
//    }
//
//    public Employee updateEmployee(Employee employee) {
//        return employeeRepository.save(employee);
//    }

    @Transactional
    public String createEmployeeWithTransaction(String name, int age, boolean secondEntityWithTransaction) {
        try {
            Employee employee = new Employee(name, age);
            employeeRepository.save(employee);

            if (secondEntityWithTransaction) {
                companyService.createCompanyWithTransaction(null, "123, ABC, DEF");
                return "Insert employee successfully with transaction and company fail with transaction";
            }
            companyService.createCompanyWithoutTransaction(null, "123, ABC, DEF");
            return "Insert employee successfully with transaction and company fail with transaction";
        }catch (UnexpectedRollbackException unexpectedRollbackException){
            return "Insert employee successfully with transaction and company fail with transaction";
        }catch (Exception e) {
            return "Insert employee successfully with transaction and company fail without transaction";
        }

    }

    public Employee createEmployeeWithoutTransaction(Long id, String name, int age) {
        Employee employee = new Employee(id, name, age);
        return employeeRepository.save(employee);
    }

    public Employee updateEmployeeEntityWithoutTransaction(Long id){
        Optional<Employee> employee = employeeRepository.findById(id);
        if(employee.isPresent()){
            employee.get().setName("Updated Name without transaction");
            return employee.get();
        }else{
            return null;
        }
    }

    @Transactional
    public Employee updateEmployeeEntityWithTransaction(Long id){
        Optional<Employee> employee = employeeRepository.findById(id);
        if(employee.isPresent()){
            employee.get().setName("Updated Name with transaction");
            return employee.get();
        }else{
            return null;
        }
    }
}
