package com.tunmii.TransactionalDemo.Service;

import com.tunmii.TransactionalDemo.Entity.Company;
import com.tunmii.TransactionalDemo.Entity.Employee;
import com.tunmii.TransactionalDemo.Repository.CompanyRepository;
import com.tunmii.TransactionalDemo.Repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CompanyService {
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

//    public Company getCompanyById(Long id) {
//        return companyRepository.findById(id);
//    }

//    public Company updateCompany(Company company) {
//        return companyRepository.save(company);
//    }


    @Transactional
    public Company createCompanyWithTransaction(String name, String address) {
        try {
            Company company = new Company(name, address);
            return companyRepository.save(company);
        }catch (Exception e){
            return null;
        }
    }

    public Company createCompanyWithoutTransaction(String name, String address){
        try {
            Company company = new Company(name, address);
            return companyRepository.save(company);
        }catch (Exception e){
            return null;
        }
    }

    public String createCompanyAndEmployeeWithoutTransaction(String companyName, String companyAddress,
                                                             String employeeName, int employeeAge) {
        String respStr = "This method will insert two tables";
        int countFail = 0;
        try{
            Company company = new Company(companyName, companyAddress);
            companyRepository.save(company);
            respStr += " which success in company";

        }catch(Exception e){
            countFail++;
            respStr += " which fail in company";
        }


        try {
            Employee employee = new Employee(employeeName, employeeAge);
            employeeRepository.save(employee);
            if(countFail == 0){
                respStr += " and in employee";
            }else{
                respStr += " but fail in employee";
            }
        }catch(Exception e){
            if(countFail == 0){
                respStr += " but fail in employee";
            }else{
                respStr += " and fail in employee";
            }

        }

        respStr += " without @Transactional";
        return respStr;
    }

    @Transactional
    public String createCompanyAndEmployeeWithTransaction(String companyName, String companyAddress,
                                                             String employeeName, int employeeAge) {

        System.out.println("In create company and employee with transaction function");
        String respStr = "This method will insert two tables";
        int countFail = 0;
        try{
            Company company = new Company(companyName, companyAddress);
            companyRepository.save(company);
            respStr += " which success in company";

        }catch(Exception e){
            countFail++;
            respStr += " which fail in company";
        }


        try {
            Employee employee = new Employee(employeeName, employeeAge);
            employeeRepository.save(employee);
            if(countFail == 0){
                respStr += " and in employee";
            }else{
                respStr += " but fail in employee";
            }
        }catch(Exception e){
            if(countFail == 0){
                respStr += " but fail in employee";
            }else{
                respStr += " and fail in employee";
            }

        }

        respStr += " with @Transactional";
        return respStr;
    }

}
