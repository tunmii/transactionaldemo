package com.tunmii.TransactionalDemo.Entity;

import jakarta.persistence.*;

@Entity
public class Company {
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private static Long count = 1L;

    @Column(nullable = false)
    private String name;
    private String address;

    public Company() {}

    public Company(String name, String address) {
        this.id = count++;
        this.name = name;
        this.address = address;
    }

    public Company(Long id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
