package com.tunmii.TransactionalDemo.Entity;

import jakarta.persistence.*;
import org.springframework.lang.NonNull;

@Entity
public class Employee {

    @Id
//    @GeneratedValue( strategy = GenerationType.AUTO)
    private Long id;

    private static Long count = 1L;

    @Column(nullable = false)
    private String name;
    private int age;

    public Employee() {}

    public Employee(String name, int age) {
        this.id = count++;
        this.name = name;
        this.age = age;
    }

    public Employee(Long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
