package com.tunmii.TransactionalDemo.Controller;

import com.tunmii.TransactionalDemo.Entity.Company;
import com.tunmii.TransactionalDemo.Entity.Employee;
import com.tunmii.TransactionalDemo.Service.CompanyService;
import com.tunmii.TransactionalDemo.Service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/main-controller")
public class MainController {

    @Autowired
    private CompanyService companyService;
    @Autowired
    private EmployeeService employeeService;

    /*
        This method will insert two tables successfully without @Transactional
     */
    @GetMapping("/insert-success-without-transaction")
    public String insertTwoTableWithoutTransaction(){
        return companyService.createCompanyAndEmployeeWithoutTransaction(
                "Company Test", "random address for test",
                "Employee Test", 25);
    }

    @GetMapping("/insert-failed-2nd-without-transaction")
    public String insertFail2ndWithoutTransaction(){
        return companyService.createCompanyAndEmployeeWithoutTransaction(
                "Company Test", "random address for this test",
                null, 22);
    }

    @GetMapping("/insert-success-with-transaction")
    public String insertTwoTableWithTransaction(){
        return companyService.createCompanyAndEmployeeWithTransaction(
                "Company Test", "random address for test",
                "Employee Test", 25);
    }

    @GetMapping("/insert-failed-2nd-with-transaction")
    public String insertFail2ndWithTransaction(){
        try{
            return companyService.createCompanyAndEmployeeWithTransaction(
                    "Company Test", "random address for this test",
                    null, 22);
        }catch (UnexpectedRollbackException unexpectedRollbackException){
            return "Transactional get error and rollback, nothing was inserted to database. \n" + unexpectedRollbackException.getMessage();
        }
    }

    @GetMapping("/update-entity-without-transaction")
    public String updateEntityWithoutTransaction(){
        employeeService.updateEmployeeEntityWithoutTransaction(1001L);
        return "update entity without transaction which doesn't make change in database!";
    }

    @GetMapping("/update-entity-with-transaction")
    public String updateEntityWithTransaction(){
        employeeService.updateEmployeeEntityWithTransaction(1002L);
        return "update entity with transaction will make change in database!";
    }

    @GetMapping("/serviceA-call-serviceB")
    public String serviceACallServiceB(@RequestParam(name = "hasTransaction") boolean hasTransaction){
        try {
            employeeService.createEmployeeWithTransaction("employee test", 22, hasTransaction);
            return "insert employee call insert company without transaction";
        }catch (UnexpectedRollbackException unexpectedRollbackException){
            return "insert employee call insert company with transaction fail all because parent transaction that directly call child method has transaction was rollback by child rollback";
        }
    }

}
