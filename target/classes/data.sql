INSERT INTO company (id, name, address) values
                                            (1001, 'Company A', '123, Le Van A street'),
                                            (1002, 'Company B', '234, Le Van B street'),
                                            (1003, 'Company C', '345, Le Van C street'),
                                            (1004, 'Company D', '456, Le Van D street');

INSERT INTO employee (id, name, age) values
                                         (1001, 'Nguyen Van A', 21),
                                         (1002, 'Nguyen Van B', 22),
                                         (1003, 'Nguyen Van C', 19),
                                         (1004, 'Nguyen Van D', 25);